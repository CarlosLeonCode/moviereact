import React from 'react';
import Paginator from 'rc-pagination';

// Scss 
import './pagination.scss';

export default function Pagination(props){

    const { totalItems, onChangePage, currentPage } = props;




    return(
        <Paginator 
            className="paginator"
            current={currentPage}
            total={totalItems}
            pageSize={20}
            onChange={onChangePage}
        />
    )
}
import React from 'react';
import { Col, Card, Icon } from 'antd';
import { IMG_PATH } from '../../utils/constants';
import { Link } from 'react-router-dom';

// styles 
import './catalog.scss';

export default function Catalog(props){
    // Data 
    const { movies: { results } } = props;

    return results.map(movie => (
        <Col key={movie.id} xs={8} className="wrapper-card-component">
            <MovieCard movie={movie}/>
        </Col>
    ));
}

// Card 
function MovieCard(props){
    const {  
        movie: { id, title, poster_path }
    } = props;
    const { Meta } = Card;
    const posterPath = `${IMG_PATH}/original/${poster_path}`;
    
    return (
        <Link to={`/movie/${id}`} className="wrapper-card-movie">
            <Card
                hoverable
                style={{width: 240}}
                cover={<img alt={title} src={posterPath} />}
            >   
                <Meta title={title} />
            </Card>
        </Link>
    )
}
import React from 'react';
import { Spin } from 'antd';

import './loader.scss';

export default function Loader(){
    return (
        <div className="wrapper-loader">
            <Spin size="large" trip="Loading..." />
        </div>
    )
}
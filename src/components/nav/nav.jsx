import React from 'react';
import { Menu } from 'antd';
import { Link } from 'react-router-dom';
import { ReactComponent as Brand } from '../../assets/img/brand.svg' 
// --
import './nav.scss';

export default function nav(){
    return(
        <div className="nav-container">
            <div className="brand">
                <Brand />
            </div>

            <Menu
                mode="horizontal"
                defaultSelectedKeys={['1']}
                style={{lineHeight: "62px", marginLeft: "8rem"}}
                className="bg-dark"
            >
                <Menu.Item key="1">
                    <Link to="/">Home</Link>        
                </Menu.Item>
                <Menu.Item key="2"> 
                    <Link to="/new_movies">New movies</Link>
                </Menu.Item>
                <Menu.Item key="3">
                    <Link to="/popular">Popular</Link>
                </Menu.Item>
            </Menu>

        </div>
    )
}
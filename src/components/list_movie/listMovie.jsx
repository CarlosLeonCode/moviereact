    import React from 'react';
import { List, Avatar, Button } from 'antd';
import { Link } from 'react-router-dom';
import Loader from '../Loader';
import { IMG_PATH } from '../../utils/constants';
import './listMovie.scss';

export default function ListMovie(props){
    const { title, movies } = props
    
    if(movies.loading || !movies.result ){
        return <Loader />
    }else{
        return (
            <div className="container-list">
                <h3 className="list-title">{ title }</h3>
                <div className="wrapper-list">
                        {
                            movies.result.results.map( movie => <Movie movie={ movie } key={ movie.id } />)
                        }
                    </div>

            </div>
        )
    }


}

function Movie(props){
    const { 
        movie: { id, title, poster_path } 
    } = props;
    const posterPath = `${ IMG_PATH }original${ poster_path }`

    return (
        <Link to={`movie/${id}`}>
            <div className="wrapper-pill">
                <div className="pill">
                    <div className="wrapper-img">
                        <img src={posterPath}></img>
                    </div>

                    <div className="wrapper-title">
                        { title }
                    </div>
                </div>
            </div>
        </Link>
    )
}
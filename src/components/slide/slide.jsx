import React from 'react';
import './slide.scss';
import { Carousel, Button, Row, Col } from 'antd';
import { IMG_PATH } from '../../utils/constants';
import { Link } from 'react-router-dom';
import Loader from '../Loader';

export default function Slide(props){

    const { movies: { loading, result } } = props;

    if (loading || !result){
        return (
            <div>
                <Loader />
            </div>
        )
    }else{
        const { results } = result;
    
        return (
            <Carousel autoplay className="slider-movies" dotPosition={'top'}>
                {results.map(movie => (
                    <Movie key={movie.id} movie={movie} />                
                ))}
            </Carousel>
        )
    }
}

function Movie(props){
    const {
        movie: { id, backdrop_path, title, overview }
    } = props;

    const imgPath = `${IMG_PATH}w500${backdrop_path}`;

    return (
        <div className="container-slide"> 
            <Row className="wrapper">
                <Col sm={24} md={12}>
                    <img src={imgPath} alt="backdrop image" className="backdrop-img" />
                </Col>
                <Col sm={24} md={12} className="details-movie">
                    <h3 className="title">{ title }</h3>
                    <p className="overview">{ overview }</p>
                    <Link to={`movie/${id}`}>
                        <Button shape="round" className="button-view">View more</Button>
                    </Link>
                </Col>
            </Row>
        </div>
    )
}
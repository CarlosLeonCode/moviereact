import React, { useEffect, useState } from 'react';
import { Modal } from 'antd';
import ReactPlayer from 'react-player';

import './modal_video.scss';

export default function ModalVideo(props){
    // --
    const { isOpen, close, videoKey, videoPlatform } = props;
    const [ urlVideo, setUrlVideo ] = useState(null);

    useEffect(() => {
        switch (videoPlatform){
            case "YouTube":
                setUrlVideo(`https://youtu.be/${videoKey}`);
                break;

            case "vimeo":
                setUrlVideo(`https://vimeo.com/${videoKey}`);
                break;
        }
    }, [videoPlatform, videoKey]);

    console.log(urlVideo);

    return (
        <Modal
            className="modal-video"
            visible={isOpen}
            centered
            onCancel={close}
            footer={false}
        >
            <ReactPlayer url={urlVideo} controls />
        </Modal>
    )
}
import React from 'react';

// packages
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import 'rc-pagination/assets/index.css'

import { Layout } from 'antd';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// Components 
import Nav from './components/nav';

// Pages 
import Home from './pages/movies/home'; 
import NewMovies from './pages/movies/new_movies'; 
import Popular from './pages/movies/popular'; 
import Search from './pages/movies/search'; 
import Show from './pages/movies/show'; 
// --
import Error404 from './pages/error404';
//--
import './App.scss';

function App() {

  const { Header, Content } = Layout;

  return (
    <Layout>
        <Router>
          <Header styele={{zIndex: 1}} className="bg-dark">
            <Nav />
          </Header>
          {/* --- */}
          <Content>
            {/* Start: routes config  */}
            <Switch>
              <Route path="/" exact={true} component={ Home } /> 
              <Route path="/new_movies" exact={ true } component={ NewMovies } />
              <Route path="/Popular" exact={ true } component={ Popular } />
              <Route path="/movie/:id" exact={ true } component={ Show } /> 
              {/* Error page */}
              <Route path="*" exact={true} component={ Error404 } />
            </Switch>
            {/* End: routes config  */}
          </Content>
          {/* --- */}
        </Router>
    </Layout>
  );
}

export default App;

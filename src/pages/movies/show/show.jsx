import React, { useState } from 'react'
import { Row, Col, Button, Modal } from 'antd';
import { useParams, Link } from 'react-router-dom';
import { API_KEY, API_PATH, IMG_PATH} from '../../../utils/constants';
import useFetch from '../../../hooks/useFetch';
import moment from 'moment';
import Loader from '../../../components/Loader';
import ModalVideo from '../../../components/modal_video';

import './show.scss';


export default function Show(){
    // Get url param
    const { id } = useParams();

    // Get data 
    const movieInfo = useFetch(
        `${API_PATH}/movie/${id}?api_key=${API_KEY}&language=es-ES`
    );
    
    if(movieInfo.loading || !movieInfo.result){
        return <Loader />
    }else{
        return < Render movieDetails={movieInfo.result} />
    }

}

function Render(props){

    const { movieDetails: {title, backdrop_path, poster_path, id } } = props;

    const [isVisible, setVisibleState] = useState(false);
    const openModal = () => setVisibleState(true);
    const closeModal = () => setVisibleState(false);
    //--
    const video = useFetch(
        `${API_PATH}/movie/${id}/videos?api_key=${API_KEY}&language=es-ES`
    )
    // Get image for background 
    const backDrop = `${IMG_PATH}/original/${backdrop_path}`;
    // -- 
    const renderVideo = () => {
        if(video.result){
            if(video.result.results.length > 0){
                return(
                    <>
                        <Button icon="play-circle" type="link" className="btn-back" onClick={openModal} >
                            View trailer
                        </Button>
                        <ModalVideo 
                            videoKey={video.result.results[0].key} 
                            videoPlatform={video.result.results[0].site}
                            isOpen={isVisible}
                            close={closeModal}
                        />
                    </>
                )
            }
        }
    }
    // --
    return(
        <div className="wrapper-movie" style={{backgroundImage: `url('${backDrop}')`}}> 
            <div className="filter-backDrop"></div>
            <Row>
                <Col span={8} offset={3} className="poster" >
                    <Poster image={poster_path}/>
                </Col>
                <Col span={10} offset={1} className="details" >
                    <Details information={props.movieDetails}/>
                    <div className="wrapper-buttons">
                        {renderVideo()}
                        <Button type="link" className="btn-back">
                            <Link to="/">Back</Link>
                        </Button>
                    </div>
                </Col>
            </Row>
        </div>
    )
}

function Poster(props){
    //--
    const {image} = props
    const imagePath = `${IMG_PATH}/original/${image}`;

    return <div style={{backgroundImage: `url('${imagePath}')`}} title="poster movie" />;
}

function Details(props){

    const { information: {id, title, release_date, overview, genres} } = props
    // --
    return (
        <div className="wrapper-details">
            <div className="header">
                <h1>{title}</h1>
            </div>
            <div className="body">
                <p>{overview.length > 0 ? overview : 'Without overview' }</p>
                <ul className="level-one">
                    <li>Release year: {moment(release_date, "YYYY-MM-DD").format("YYYY")}</li>
                    <li>Genres: 
                        <ul className="level-two">
                            {genres.map(gender => (
                                <li key={gender.id} >{gender.name}</li>
                            ))}
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    )
}
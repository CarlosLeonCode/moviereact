import React, { useState, useEffect } from 'react'
import { Row, Col, Input } from 'antd';
import { withRouter } from 'react-router-dom';
import Catalog from '../../../components/catalog ';
import queryString from 'query-string';

// Constants 
import { API_PATH, API_KEY } from '../../../utils/constants';
// Styles 
import './search.scss'; 


function Search(props){
    const { location, history } = props;
    const [ movieList, setMovieList ] = useState([]);
    const [ paramQuery, setParamQuery ]  = useState('');
    
    // Hook
    useEffect(() => {
        (async () => {
            // function for get params from url with location 
            const paramsSearch = queryString.parseUrl(location.search);
            const { q } = paramsSearch.query;
            const response = await fetch(`${API_PATH}/search/movie?api_key=${API_KEY}&language=es-ES&query=${q}&page=1`)
            // Parse to json 
            const result = await response.json();
            // Set states 
            setParamQuery(q);
            setMovieList(result)
        })();
    },[location.search])

    // For change input value 
    const onSearchMovie = event => {
        const currentParams = queryString.parse(location.search)
        // Update param url
        currentParams.q = event.target.value;
        // Use history from withRouter 
        history.push(`?${queryString.stringify(currentParams)}`)
        setParamQuery(event.target.value)
    }

    return (
        <Row className="bg-app" > 
            <Col span={12} offset={6} className="wrapper-search">
                <h5>Search you favorite movie</h5>
                <Input value={paramQuery} onChange={onSearchMovie} />
            </Col>
            <Col span={12} className="resultSearch">
                {movieList.results && (
                    <Row className="movies"> 
                        <Col span={24}>
                            <Catalog movies={movieList} />
                        </Col>
                    </Row>
                )}
            </Col>
        </Row>

    )
}

export default withRouter(Search)
import React, { useState, useEffect } from 'react';
import { Row, Col } from 'antd';
import { API_PATH, API_KEY } from '../../../utils/constants';
import Loader from '../../../components/Loader';
import Catalog from '../../../components/catalog ';
import Pagination from '../../../components/pagination';
// Styles 
import './popular.scss';
export default function NewMovies(){
    // States 
    const [ page, setPage ] = useState(1);
    const [ movieList, setMovieList ] = useState([]);

    // Use effect
    useEffect(() => {
        (async () => {
            const response = await fetch(
                `${API_PATH}/movie/popular?api_key=${API_KEY}&languages=es-ES&page=${page}`
            );
            const movies = await response.json();
            setMovieList(movies);
        })();
    },[page])


    const onChangePage = page => (setPage(page));

    console.log(movieList);

    return(
        <Row className="container-movie-list">
            <div className="card">
                <Col span={24} className="c-header">
                    <h1>Popular movies</h1>
                </Col>
                <Col span={24} className="c-body">
                    <Row className="row">
                        { movieList.results ? (
                            <>
                                <Col span={24}>
                                    <Row>
                                        <Catalog movies={movieList}/>
                                    </Row>
                                </Col>
                                <Col span={24}>
                                    <Pagination 
                                        currentPage={movieList.page}
                                        totalItems={movieList.total_results}
                                        onChangePage={onChangePage}
                                    />
                                </Col>
                            </>
                        ) : (
                            <Col span={24}>
                                <Loader />
                            </Col>
                        )}
                    </Row> 
                </Col>  
            </div>
        </Row>
    )
}
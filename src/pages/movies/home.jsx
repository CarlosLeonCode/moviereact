import React from 'react';

// Components 
import Slide from '../../components/slide';
import List from '../../components/list_movie';
import { Row, Col } from 'antd';
import Footer from '../../components/footer';

import useFetch from '../../hooks/useFetch';
import {API_KEY,API_PATH} from '../../utils/constants';

export default function Home(){

    const moviesResponse = useFetch(
        `${API_PATH}/movie/popular?api_key=${API_KEY}&language=es&page=1`
    );

    const moviesList = useFetch(
        `${API_PATH}/movie/now_playing?api_key=${API_KEY}&language=es`
    );

    const topRatedList = useFetch(
        `${API_PATH}/movie/top_rated?api_key=${API_KEY}&language=es`
    );

    return(
        <div className="bg-app">
            <Slide  movies={moviesResponse}/>
            <Row>
                <Col span={24} className="wrapper-list">
                    <List movies={moviesList} title="Popular movies"/>
                </Col>
                <Col span={24} className="wrapper-list">
                    <List movies={topRatedList} title="Más valoradas"/>
                </Col>
            </Row>
            {/* --- */}
            <Footer />
        </div>
    )
} 